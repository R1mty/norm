<?php

return [
    'secure' => [
        'dir'     => __DIR__ . '/../keys/',
        'private' => 'private.key',
        'public'  => 'public.key',
    ],

    'db' => [
        'driver'   => 'mysql',
        'host'     => 'localhost',
        'port'     => '3306',
        'dbname'   => 'laba',
        'user'     => 'mysql',
        'password' => 'mysql',
    ],

    'rabbit' => [
        'queue_name' => 'queue',
        'host'       => 'localhost',
        'port'       => 5672,
        'user'       => 'guest',
        'password'   => 'guest',
    ],

    'socket' => [
        'host' => '127.0.0.1',
        'port' => 8880,
    ],
];
