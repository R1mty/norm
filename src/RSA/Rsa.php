<?php

namespace App\RSA;

class Rsa
{
    /**
     * @var string
     */
    private $privateKey;
    /**
     * @var string
     */
    private $publicKey;

    public function __construct(array $config)
    {
        $this->privateKey = file_get_contents($config['dir'] . $config['private']);
        $this->publicKey = file_get_contents($config['dir'] . $config['public']);
    }

    public function getPublicKey(): string
    {
        return $this->publicKey;
    }

    public function encode($data, string $publicKey)
    {
        $publicKeyResource = openssl_pkey_get_public($publicKey);

        openssl_public_encrypt(
            $data,
            $encrypted,
            $publicKeyResource
        );

        openssl_free_key($publicKeyResource);

        return base64_encode($encrypted);
    }

    public function decode($encrypted)
    {
        $privateKey = openssl_pkey_get_private($this->privateKey);

        $encrypted = base64_decode($encrypted);

        openssl_private_decrypt(
            $encrypted,
            $decrypted,
            $privateKey
        );

        openssl_free_key($privateKey);

        return $decrypted;
    }
}
