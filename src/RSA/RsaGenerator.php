<?php

namespace App\RSA;

class RsaGenerator
{

    /**
     * @var string
     */
    private $privateKey;
    /**
     * @var string
     */
    private $publicKey;

    public function __construct(array $config)
    {
        $this->privateKey = $config['dir'] . $config['private'];
        $this->publicKey = $config['dir'] . $config['public'];
    }


    /**
     * @param $keyResource
     */
    public function generatePublicKey($keyResource)
    {
        // получаем публичный ключ
        $publicKey = openssl_pkey_get_details($keyResource);

        // сохраняем публичный ключ в файл
        file_put_contents(
            $this->publicKey,
            $publicKey['key']
        );
    }

    public function generate()
    {
        $keyResource = $this->generatePrivateKey();
        $this->generatePublicKey($keyResource);
    }

    public function generatePrivateKey()
    {
        $keyResource = openssl_pkey_new(
            [
                'private_key_bits' => 2048,
                'private_key_type' => OPENSSL_KEYTYPE_RSA,
            ]
        );

        openssl_pkey_export($keyResource, $privateKey);

        // сохраняем приватный ключ в файл
        file_put_contents($this->privateKey, $privateKey);

        return $keyResource;
    }
}