<?php

namespace App\DES;

class Des
{
    private $cipher;
    private $key;

    public function __construct($key)
    {
        // Указываем алгоритм шифрования
        $this->cipher = 'des-cbc';

        // Сразу проверим, что алгоритм нам доступен
        // https://www.php.net/manual/ru/function.openssl-get-cipher-methods.php
        if (!in_array($this->cipher, openssl_get_cipher_methods(), true)) {
            throw new \Exception('Алгоритм шифрования не найден');
        }

        $this->key = $key;
    }

    public function encrypt($data): string
    {
        $plaintext = $data;

        try {
            // Получаем длину последовательности, необходимой для выбранного алгоритма
            $ivlen = openssl_cipher_iv_length($this->cipher);

            // Получаем случайную строку, необходимой длины
            $iv = random_bytes($ivlen);

            // Шифруем исходное сообщение
            $ciphertext = openssl_encrypt($plaintext, $this->cipher, $this->key, 0, $iv);
        } catch (\Exception $e) {
            echo $e->getMessage();
            throw $e;
        }

        // Возвращаем зашифрованный текст вместе со случайной строкой (которая использовалась для шифрования), разделённые разделителем [:]
        // чтобы была возможность расшифровать дальше
        return bin2hex($iv) . '[:]' . $ciphertext;
    }

    public function decrypt($data)
    {
        $parts = explode('[:]', $data); // Разбиваем зашифрованную строку по разделителю
        if (count($parts) !== 2) {
            throw new \Exception('На вход пришла некорректная строка');
        }

        $iv = hex2bin($parts[0]); // Восстанавливаем случайную строку, которую получили при шифровании
        $encrypted = $parts[1];

        return openssl_decrypt($encrypted, $this->cipher, $this->key, $options = 0, $iv);
    }
}
