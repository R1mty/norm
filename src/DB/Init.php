<?php

namespace App\DB;

class Init
{
    /**
     * @var \PDO
     */
    private $PDO;

    public function __construct(\PDO $PDO)
    {
        $this->PDO = $PDO;
    }

    public function init()
    {
        $this->createDenormalized();

        $this->createNormalized();

        $this->fillDenormalized();
    }

    private function createDenormalized()
    {
        $denormalized = "
        CREATE TABLE IF NOT EXISTS denormalized_measurements (
	id INT UNSIGNED auto_increment NOT NULL,
	city_name VARCHAR(255) NOT NULL COMMENT 'Город',
	building_name VARCHAR(255) NOT NULL COMMENT 'Дом',
	apartment_number VARCHAR(255) NOT NULL COMMENT 'Номер квартиры',
	measurement_date DATE NOT NULL COMMENT 'Дата показаний',
	measurement_type VARCHAR(255) NOT NULL COMMENT 'Тип счётчика',
	measurement_value FLOAT NULL COMMENT 'Показания',
	CONSTRAINT denormalized_measurements_PK PRIMARY KEY (id)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_unicode_ci;
        ";

        $this->PDO->exec($denormalized);
    }

    private function createNormalized()
    {
        $city = "CREATE TABLE IF NOT EXISTS city (
	id INT auto_increment NOT NULL,
	`name` VARCHAR(255) NULL,
	CONSTRAINT city_PK PRIMARY KEY (id)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_unicode_ci;
";
        $this->PDO->exec($city);

        $building = "CREATE TABLE IF NOT EXISTS building (
	id INT auto_increment NOT NULL,
	city_id INT NOT NULL,
	`name` VARCHAR(255) NOT NULL,
	CONSTRAINT building_PK PRIMARY KEY (id),
	CONSTRAINT building_city_FK FOREIGN KEY (city_id) REFERENCES city(id)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_unicode_ci;
        ";
        $this->PDO->exec($building);


        $apartment = "CREATE TABLE IF NOT EXISTS apartment (
	id INT auto_increment NOT NULL,
	building_id INT NOT NULL,
	`number` VARCHAR(255) NOT NULL COMMENT 'Номер квартиры',
	CONSTRAINT apartment_PK PRIMARY KEY (id),
	CONSTRAINT apartment_building_FK FOREIGN KEY (building_id) REFERENCES building(id)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_unicode_ci;
        ";
        $this->PDO->exec($apartment);

        $type = "CREATE TABLE IF NOT EXISTS measurement_type (
	id INT auto_increment NOT NULL,
	`name` VARCHAR(255) NOT NULL,
	CONSTRAINT measurement_type_PK PRIMARY KEY (id)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_unicode_ci;
        ";
        $this->PDO->exec($type);


        $type = "CREATE TABLE IF NOT EXISTS measurement (
	id INT auto_increment NOT NULL,
	apartment_id INT NOT NULL,
	`date` DATE NOT NULL COMMENT 'Дата показаний',
	type_id INT NOT NULL COMMENT 'Тип счётчика',
	`value` FLOAT NULL COMMENT 'Показания',
	CONSTRAINT measurement_PK PRIMARY KEY (id),
	CONSTRAINT measurement_apartment_FK FOREIGN KEY (apartment_id) REFERENCES apartment(id),
	CONSTRAINT measurement_type_FK FOREIGN KEY (type_id) REFERENCES measurement_type(id)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_unicode_ci;
        ";
        $this->PDO->exec($type);
    }

    private function fillDenormalized()
    {
        $sql = 'SELECT COUNT(*) AS cnt FROM denormalized_measurements';
        $res = $this->PDO->query($sql);
        $count = (int)$res->fetchColumn();

        if ($count > 0) {
            echo 'Таблица уже заполнена' . PHP_EOL;

            return;
        }

        $sql = "
        INSERT INTO denormalized_measurements (city_name,building_name,apartment_number,measurement_date,measurement_type,measurement_value) VALUES
	 ('Пермь','Чкалова 9','1','2021-03-01','Холодная вода',10.0),
	 ('Пермь','Чкалова 9','1','2021-03-01','Горячая вода',20.0),
	 ('Пермь','Чкалова 9','1','2021-03-01','Электричество',30.0),
	 ('Пермь','Чкалова 9','1','2021-04-01','Холодная вода',21.0),
	 ('Пермь','Чкалова 9','1','2021-04-01','Горячая вода',32.0),
	 ('Пермь','Чкалова 9','1','2021-04-01','Электричество',43.0),
	 ('Пермь','Чкалова 10','1','2021-03-01','Холодная вода',10.0),
	 ('Пермь','Чкалова 10','1','2021-03-01','Горячая вода',20.0),
	 ('Пермь','Чкалова 10','1','2021-03-01','Электричество',30.0),
	 ('Пермь','Чкалова 10','2','2021-04-01','Холодная вода',21.0),
	 ('Пермь','Чкалова 10','2','2021-04-01','Горячая вода',32.0),
	 ('Пермь','Чкалова 10','2','2021-04-01','Электричество',43.0),
	 ('Пермь','Чкалова 11','1А','2021-04-01','Газ',19.4),
	 ('Пермь','Чкалова 11','1А','2021-04-02','Электричество',10.0);
        ";

        $this->PDO->exec($sql);
        $sql = "
        INSERT INTO denormalized_measurements (city_name,building_name,apartment_number,measurement_date,measurement_type,measurement_value) VALUES
	 ('Москва','Акулова 5','1','2021-03-01','Холодная вода',19.0),
	 ('Москва','Акулова 5','1','2021-03-01','Горячая вода',30.0),
	 ('Москва','Акулова 5','1','2021-03-01','Электричество',30.0),
	 ('Москва','Акулова 5','1','2021-04-01','Холодная вода',31.0),
	 ('Москва','Акулова 5','1','2021-04-01','Горячая вода',33.0),
	 ('Москва','Акулова 5','1','2021-04-01','Электричество',43.0),
	 ('Москва','Строителей 8','1','2021-03-01','Холодная вода',19.0),
	 ('Москва','Строителей 8','1','2021-03-01','Горячая вода',30.0),
	 ('Москва','Строителей 8','1','2021-03-01','Электричество',30.0),
	 ('Москва','Строителей 8','3','2021-04-01','Холодная вода',31.0),
	 ('Москва','Строителей 8','3','2021-04-01','Горячая вода',33.0),
	 ('Москва','Строителей 8','3','2021-04-01','Электричество',43.0),
	 ('Москва','Снайперов 11','1','2021-04-01','Газ',19.4),
	 ('Москва','Снайперов 11','1','2021-04-03','Электричество',19.0);
        ";

        $this->PDO->exec($sql);
    }
}
