<?php

namespace App\DB;

class Importer
{
    /**
     * @var \PDO
     */
    private $PDO;

    public function __construct(\PDO $PDO)
    {
        $this->PDO = $PDO;
    }

    public function import($data)
    {
        echo 'Импортируем' . PHP_EOL;

        unset($data['id']);
        echo json_encode($data, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT) . PHP_EOL . PHP_EOL;

        $city_id = $this->createOrGetCityID($data['city_name']);
        $building_id = $this->createOrGetBuildingID($city_id, $data['building_name']);
        $apartment_id = $this->createOrGetApartmentID($building_id, $data['apartment_number']);
        $type_id = $this->createOrGetTypeID($data['measurement_type']);
        $this->createMeasurementIfNotExists(
            $apartment_id,
            $data['measurement_date'],
            $type_id,
            $data['measurement_value']
        );
        echo '----------------------------' . PHP_EOL . PHP_EOL;
    }


    private function createOrGetCityID(string $city_name): int
    {
        $sql = 'SELECT id FROM city WHERE `name` = ? ORDER BY id ASC LIMIT 1';
        $st = $this->PDO->prepare($sql);
        if ($st->execute([$city_name])) {
            $row = $st->fetch();
            if ($row) {
                echo 'Город ' . $city_name . ' уже существует' . PHP_EOL;

                return $row['id'];
            }
        }

        $sql = 'INSERT INTO city (`name`) VALUES (?)';
        $st = $this->PDO->prepare($sql);
        if ($st->execute([$city_name])) {
            echo 'Город ' . $city_name . ' создан' . PHP_EOL;

            return $this->PDO->lastInsertId();
        }

        throw new \Exception('Что-то пошло не так. Не удалось создать город');
    }


    private function createOrGetBuildingID(int $city_id, string $building_name): int
    {
        $sql = 'SELECT id FROM building WHERE city_id = ? AND `name` = ? ORDER BY id ASC LIMIT 1';
        $st = $this->PDO->prepare($sql);
        if ($st->execute([$city_id, $building_name])) {
            $row = $st->fetch();
            if ($row) {
                echo 'Дом ' . $building_name . ' уже существует' . PHP_EOL;

                return $row['id'];
            }
        }

        $sql = 'INSERT INTO building (city_id, `name`) VALUES (?, ?)';
        $st = $this->PDO->prepare($sql);
        if ($st->execute([$city_id, $building_name])) {
            echo 'Дом ' . $building_name . ' создан' . PHP_EOL;

            return $this->PDO->lastInsertId();
        }

        throw new \Exception('Что-то пошло не так. Не удалось создать дом');
    }

    private function createOrGetApartmentID(int $building_id, string $apartment_number): int
    {
        $sql = 'SELECT id FROM apartment WHERE building_id = ? AND `number` = ? ORDER BY id ASC LIMIT 1';
        $st = $this->PDO->prepare($sql);
        if ($st->execute([$building_id, $apartment_number])) {
            $row = $st->fetch();
            if ($row) {
                echo 'Квартира ' . $apartment_number . ' уже существует в доме' . PHP_EOL;

                return $row['id'];
            }
        }

        $sql = 'INSERT INTO apartment (building_id, `number`) VALUES (?, ?)';
        $st = $this->PDO->prepare($sql);
        if ($st->execute([$building_id, $apartment_number])) {
            echo 'Квартира ' . $apartment_number . ' создана' . PHP_EOL;

            return $this->PDO->lastInsertId();
        }

        throw new \Exception('Что-то пошло не так. Не удалось создать квартиру');
    }

    private function createOrGetTypeID($measurement_type)
    {
        $sql = 'SELECT id FROM measurement_type WHERE `name` = ? ORDER BY id ASC LIMIT 1';
        $st = $this->PDO->prepare($sql);
        if ($st->execute([$measurement_type])) {
            $row = $st->fetch();
            if ($row) {
                echo 'Вид измерения ' . $measurement_type . ' уже существует' . PHP_EOL;

                return $row['id'];
            }
        }

        $sql = 'INSERT INTO measurement_type (`name`) VALUES (?)';
        $st = $this->PDO->prepare($sql);
        if ($st->execute([$measurement_type])) {
            echo 'Вид измерения ' . $measurement_type . ' создан' . PHP_EOL;

            return $this->PDO->lastInsertId();
        }

        throw new \Exception('Что-то пошло не так. Не удалось создать вид измерений');
    }

    private function createMeasurementIfNotExists($apartment_id, $date, $type_id, $value): void
    {
        $sql = '
SELECT id FROM measurement 
WHERE apartment_id =? AND `date` = ? AND type_id = ?;';
        $st = $this->PDO->prepare($sql);
        if ($st->execute([$apartment_id, $date, $type_id])) {
            $row = $st->fetch();
            if ($row) {
                $sql = 'UPDATE measurement SET `value` = ? WHERE id = ?';
                $st = $this->PDO->prepare($sql);
                $st->execute([$value, $row['id']]);
                echo 'Измерение уже имеется' . PHP_EOL;

                return;
            }
        }

        $sql = 'INSERT INTO measurement
(apartment_id, `date`, type_id, value)
VALUES(?, ?, ?, ?)';
        $st = $this->PDO->prepare($sql);
        if ($st->execute([$apartment_id, $date, $type_id, $value])) {
            echo 'Измерение занесено' . PHP_EOL;

            return;
        }

        throw new \Exception('Что-то пошло не так. Не удалось создать запись об измерении');
    }
}
