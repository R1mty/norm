<?php

namespace App\Rabbit;

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class Writer
{
    /**
     * @var AMQPChannel
     */
    private $channel;
    /**
     * @var string
     */
    private $queue;

    public function __construct(AMQPStreamConnection $connection, string $queue)
    {
        $channel = $connection->channel();
        $channel->queue_declare($queue, false, false, false, false);
        $this->channel = $channel;
        $this->queue = $queue;
    }

    public function write(string $message): void
    {
        $msg = new AMQPMessage($message);
        $this->channel->basic_publish($msg, '', $this->queue);
    }
}
