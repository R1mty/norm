<?php

namespace App\Rabbit;

use App\DB\Importer;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class Consumer
{
    /**
     * @var AMQPChannel
     */
    private $channel;
    /**
     * @var Importer
     */
    private $importer;

    public function __construct(AMQPStreamConnection $connection, string $queue, Importer $importer)
    {
        $channel = $connection->channel();
        $channel->queue_declare($queue, false, false, false, false);
        $this->channel = $channel;
        $this->importer = $importer;
    }

    public function consume(): void
    {
        $this->channel->basic_consume(
            '',
            '',
            false,
            true,
            false,
            false,
            function (AMQPMessage $message) {
                /**
                 * city_name
                 * building_name
                 * apartment_number
                 * measurement_date
                 * measurement_type
                 * measurement_value
                 */
                $data = json_decode($message->body, true);
                $this->importer->import($data);
            }
        );

        while (true) {
            $this->channel->wait();
        }
    }

}
