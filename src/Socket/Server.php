<?php

namespace App\Socket;

use App\DES\Des;
use App\Rabbit\Writer;
use App\RSA\Rsa;

class Server
{
    private $host;
    private $port;
    /**
     * @var Rsa
     */
    private $rsa;
    /**
     * @var Writer
     */
    private $writer;

    public function __construct($config, Rsa $rsa, Writer $writer)
    {
        $this->host = $config['host'];
        $this->port = $config['port'];
        $this->rsa = $rsa;
        $this->writer = $writer;
    }

    // https://www.php.net/manual/ru/sockets.examples.php
    public function serve()
    {
        /* Позволяет скрипту ожидать соединения бесконечно. */
        set_time_limit(0);

        $host = $this->host;
        $port = $this->port;
        $des = null;

        if (($sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) === false) {
            echo "Не удалось выполнить socket_create(): причина: " . socket_strerror(socket_last_error()) . PHP_EOL;

            return;
        }

        if (socket_bind($sock, $host, $port) === false) {
            echo "Не удалось выполнить socket_bind(): причина: " . socket_strerror(socket_last_error($sock)) . PHP_EOL;

            return;
        }

        if (socket_listen($sock) === false) {
            echo "Не удалось выполнить socket_listen(): причина: " . socket_strerror(
                    socket_last_error($sock)
                ) . PHP_EOL;

            return;
        }

        echo "Сервер готов" . PHP_EOL;
        do {
            echo 'Ожидание новых данных' . PHP_EOL;

            if (($msgsock = socket_accept($sock)) === false) {
                echo "Не удалось выполнить socket_accept(): причина: " . socket_strerror(
                        socket_last_error($sock)
                    ) . PHP_EOL;
                break;
            }

            echo 'Данные получены' . PHP_EOL;

            do {
                $fullMessage = $this->read($msgsock);

                $decoded = json_decode($fullMessage, true);

                $command = $decoded['command'];

                switch ($command) {
                    case 'get-key':
                        echo 'Возвращаем публичный ключ' . PHP_EOL;
                        $publicKeyContent = $this->rsa->getPublicKey();
                        $this->write($msgsock, $publicKeyContent);
                        break;

                    case 'set-key':
                        try {
                            echo 'Устанавливаем DES ключ' . PHP_EOL;

                            echo 'Получили:' . $decoded['key'] . PHP_EOL;

                            $desKey = $this->rsa->decode($decoded['key']);
                            $des = new Des($desKey);

                            echo 'Расшифрованный DES ключ ' . $desKey . PHP_EOL;

                            echo 'Успех' . PHP_EOL;
                            $this->write($msgsock, 'OK');
                        } catch (\Exception $e) {
                            $this->write($msgsock, $e->getMessage());
                        }
                        break;

                    case 'import':
                        if ($des === null) {
                            $this->write($msgsock, 'Ключ шифрования не установлен');
                            break;
                        }

                        echo 'Зашифрованные данные: ' . $decoded['data'] . PHP_EOL;

                        $data = $des->decrypt($decoded['data']);

                        echo 'Расшифрованные данные: ' . $data . PHP_EOL;

                        $this->writer->write($data);


                        break;

                    case 'finish':
                        echo 'Заканчиваем работу с клиентом' . PHP_EOL;
                        break 2;
                }
            } while (true);
            socket_close($msgsock);
        } while (true);

        socket_close($sock);
    }

    /**
     * @param $socket
     * @param string $message
     *
     * @return false|int
     */
    private function write($socket, string $message)
    {
        $message .= "\r\n"; // Чтобы клиент понимал, когда сообщение завершилось добавляем перенос строки

        return socket_write($socket, $message, strlen($message));
    }


    /**
     * @param $socket
     *
     * @return string
     */
    private function read($socket): string
    {
        $message = '';
        $start = 0;
        while ($out = socket_read($socket, 2048, PHP_NORMAL_READ)) {
            if (empty(trim($out))) {
                if ($start === 0) {
                    continue;
                }

                break;
            }

            $start = 1;

            $message .= $out;
        }

        return $message;
    }
}
