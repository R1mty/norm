<?php

namespace App\Socket;

use App\DES\Des;
use App\Rabbit\Writer;
use App\RSA\Rsa;

class Client
{
    private $host;
    private $port;
    /**
     * @var Rsa
     */
    private $rsa;
    /**
     * @var \PDO
     */
    private $PDO;

    public function __construct($config, Rsa $rsa, \PDO $PDO)
    {
        $this->host = $config['host'];
        $this->port = $config['port'];
        $this->rsa = $rsa;
        $this->PDO = $PDO;
    }

    /**
     * https://www.php.net/manual/ru/sockets.examples.php
     */
    public function run(): void
    {
        /* Позволяет скрипту ожидать соединения бесконечно. */
        set_time_limit(0);

        $host = $this->host;
        $port = $this->port;
        echo "Новый клиент. Соединение TCP/IP" . PHP_EOL;

        /* Создаём сокет TCP/IP. */
        $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

        if ($socket === false) {
            echo "Не удалось выполнить socket_create(): причина: " . socket_strerror(socket_last_error()) . PHP_EOL;
            return;
        }

        echo "OK." . PHP_EOL;

        echo "Пытаемся соединиться с '$host' на порту '$port'..." . PHP_EOL;

        $result = socket_connect($socket, $host, $port);

        if ($result === false) {
            $error = socket_strerror(socket_last_error($socket));
            echo "Не удалось выполнить socket_connect()." . PHP_EOL;
            echo "Причина: " . $error . PHP_EOL;
            return;
        }

        echo "OK.\n";


        echo "Отправляем запрос на получение публичного ключа" . PHP_EOL;
        $message = ['command' => 'get-key'];
        $this->write($socket, $message);
        echo "OK.\n";

        echo "Читаем ответ:" . PHP_EOL . PHP_EOL;

        $publicKey = $this->read($socket);

        echo "Получили ключ: " . PHP_EOL;

        echo $publicKey . PHP_EOL . PHP_EOL;

        $desKey = random_bytes(32);

        echo 'Сгенерировали DES ключ: ' . $desKey . PHP_EOL;

        $encodedDesKey = $this->rsa->encode($desKey, $publicKey);

        echo 'DES ключ после шифрования: ' . $encodedDesKey . PHP_EOL;

        echo 'Передаём ключ на сервер' . PHP_EOL;

        $this->write(
            $socket,
            [
                'command' => 'set-key',
                'key'     => $encodedDesKey,
            ]
        );

        $response = $this->read($socket);
        if (trim($response) === 'OK') {
            echo 'Сервер успешно прописал ключ' . PHP_EOL . PHP_EOL;

            $des = new Des($desKey);

            echo 'Начинаем шифрованную передачу' . PHP_EOL;

            $sql = "SELECT * FROM denormalized_measurements";
            $res = $this->PDO->query($sql);
            $rows = $res->fetchAll();
            foreach ($rows as $row) {
                $this->write(
                    $socket,
                    [
                        'command' => 'import',
                        'data'    => $des->encrypt(json_encode($row)),
                    ]
                );
            }
            echo 'Данные переданы' . PHP_EOL . PHP_EOL;
        } else {
            echo 'Сервер ответил ошибкой' . PHP_EOL;
            echo $response . PHP_EOL;
        }

        $this->write(
            $socket,
            [
                'command' => 'finish',
            ]
        );

        echo "Закрываем сокет..." . PHP_EOL;
        socket_close($socket);
        echo "OK.\n\n";
    }

    /**
     * @param $msgsock
     * @param string $talkback
     *
     * @return false|int
     */
    private function write($msgsock, $data)
    {
        $talkback = json_encode($data) . "\r\n";

        return socket_write($msgsock, $talkback, strlen($talkback));
    }

    /**
     * @param $socket
     *
     * @return string
     */
    private function read($socket): string
    {
        $message = '';
        $start = 0;
        while ($out = socket_read($socket, 2048, PHP_NORMAL_READ)) {
            if (empty(trim($out))) {
                if ($start === 0) {
                    continue;
                }

                break;
            }

            $start = 1;

            $message .= $out;
        }

        return $message;
    }
}
