<?php

use App\DB\Importer;
use App\DB\Init;
use App\Rabbit\Consumer;
use App\Rabbit\Writer;
use App\RSA\Rsa;
use App\Socket\Client;
use App\Socket\Server;
use App\RSA\RsaGenerator;
use PhpAmqpLib\Connection\AMQPStreamConnection;

require_once __DIR__ . '/vendor/autoload.php';

$config = include __DIR__ . '/config/config.php';

$dbConfig = $config['db'];

$dns = sprintf(
    "%s:host=%s;port=%s;dbname=%s",
    $dbConfig['driver'],
    $dbConfig['host'],
    $dbConfig['port'],
    $dbConfig['dbname']
);

$dbOptions = [
    \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC, // Чтобы в результате получать массивы
    \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION, // Чтобы ошибки бросали исключения
];

$pdo = new PDO($dns, $dbConfig['user'], $dbConfig['password'], $dbOptions); // Подключаемся к базе данных

// Подключаемся к серверу очередей
$rabbit = new AMQPStreamConnection(
    $config['rabbit']['host'],
    $config['rabbit']['port'],
    $config['rabbit']['user'],
    $config['rabbit']['password']
);

if ($argv) {
    // удаляем первый элемент массива (index.php)
    array_shift($argv);
    if (empty($argv)) {
        echo 'Команда не указана' . PHP_EOL;
        die;
    }

    $command = $argv[0];

    $queue_name = $config['rabbit']['queue_name'];

    switch ($command) {
        case 'generate-keys':
            $rsa = new RsaGenerator($config['secure']);
            $rsa->generate();
            break;

        case 'init-db':
            (new Init($pdo))->init();
            break;

        case 'server':
            $rsa = new Rsa($config['secure']);
            $writer = new Writer($rabbit, $queue_name);
            $server = new Server($config['socket'], $rsa, $writer);
            $server->serve();
            break;

        case 'client':
            $rsa = new Rsa($config['secure']);
            $writer = new Writer($rabbit, $queue_name);
            $client = new Client($config['socket'], $rsa, $pdo);
            $client->run();
            break;

        case 'consume':
            $importer = new Importer($pdo);
            $c = new Consumer($rabbit, $queue_name, $importer);
            $c->consume();
            break;

        default:
            echo 'Команда не найдена' . PHP_EOL;
    }
}

$rabbit->close();
